/*
* Module Dependecies 
*/
var express = require('express')
var app = express()
var port = 3000

app.use(express.static('./dist'))
app.set('view engine', 'jade')
app.set('views', './views')

app.get('/', function (req, res) {
    res.render('index')
  })

app.listen(port, function(){
    console.log('Escuchando en el puerto : ' + port)
  })
