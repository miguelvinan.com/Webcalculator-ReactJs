import React, {Component} from 'react'
import Calculator from '../components/Calculator'
import Time from '../components/Time'

class Home extends Component {
  render() {
    return (
      <div>
        <section className="Grid">
          <div className="Grid-item"><span>1</span></div>
          <div className="Grid-item"><span>2</span></div>
          <div className="Grid-item"><span>3</span></div>
          <div className="Grid-item"><span>4</span></div>
          <div className="Grid-item"><span>5</span></div>
          <div className="Grid-item"><span>6</span></div>
          <div className="Grid-item"><span>7</span></div>
        </section>
        <section className="Grid">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facilis velit quos in quasi impedit fuga distinctio eaque, aut, dicta hic, at natus vel temporibus doloremque totam! Modi iste nostrum odit!</p>
        </section>
        <section className="Grid">
          <Time />
        </section>
      </div>
    );
  }
}

export default Home
