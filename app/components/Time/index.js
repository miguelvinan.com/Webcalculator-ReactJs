// Styles
import './styles.post.sass'

/**
 * Module Dependecies
 */
import React, {Component} from 'react';

class Time extends Component {
  render() {
    return (
      <div className="Time">
        <label>Tiempo</label>
        <input name="time" className="P(10px)" type="input" placeholder="Escribe aquí lo que quieras..."/>
      </div>
    );
  }
}

export default Time;
