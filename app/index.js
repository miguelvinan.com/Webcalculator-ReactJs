// Styles
import './styles/app.post.sass'

/*
* Modules Dependencies
*/
import React from 'react'
import ReactDom from 'react-dom'
import Home from './routes/Home'

class App extends React.Component {
  render() {
    return <Home />
  }
}

ReactDom.render(
  <App />,
  document.getElementById('app')
)