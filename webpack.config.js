/*
 * Module Dependencies
 * */

var webpack = require('webpack')
var path = require('path')
var ExtractTextPlugin = require("extract-text-webpack-plugin")
var extractCSS = new ExtractTextPlugin('css/[name].css')

// Plugins POSTCSS
var lost = require('lost') //Grilla
var precss = require('precss') //Sintaxis como SASS 
var autoprefixer = require('autoprefixer') //autoprefixer
var rucksackCSS = require('rucksack-css') //font-size responsive
var postcssLinter = require('postcss-bem-linter') //Linter para escritura SUITCSS
var mediasMinMax = require('postcss-media-minmax') //Abreviación de sintaxis en los Media queries con >= o <=
var customMedias = require('postcss-custom-media') //Crea media queries custom como: @custom-media --mobile (min-width: 500px)
var cssNesting = require('postcss-nesting') //Poner mediaqueries dentro de los componentes
var cssImports = require('postcss-import') //Imports in Css
var cssnano = require('cssnano')

// Routes
var APP_DIR = path.join(__dirname, 'app') //Entry
var BUILD_DIR = path.join(__dirname, 'dist') //Outpout

var config = {
  entry: APP_DIR,
  cache: false,
  output: {
    path: BUILD_DIR,
    filename: 'js/[name].bundle.js'
  },
  module: {
    loaders: [{
      test: /\.(js|jsx)$/,
      include: APP_DIR,
      exclude: /(node_modules)/,
      loader: "babel-loader"
    },{
      test: /\.sass$/,
      loader: extractCSS.extract(['css?minimize','postcss','sass'])
    }]
  },
  postcss: function () {
      return {
        plugins: [
          autoprefixer,
          // precss,
          // cssnano,
          lost,
          rucksackCSS,
          mediasMinMax,
          customMedias,
          // cssNesting,
          // cssImports
        ]
      }
    },
  plugins: [extractCSS]
}

module.exports = config;